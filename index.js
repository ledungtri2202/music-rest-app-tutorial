const express = require('express');
const connection = require('./db/connect');
const router = require('./routes/route');

const backend = express();
backend.use(express.json());

backend.get('/', function (req, res) {
    res.redirect('/music');
});

backend.use('/api', router);

backend.listen(3001, function () {
    console.log("Server started successfully...")
});