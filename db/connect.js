const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/music_app', { useNewUrlParser: true })
    .catch(error => console.error('Connection error.', error.message));

module.exports = mongoose.connection;


