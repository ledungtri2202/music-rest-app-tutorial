const Album = require('../db/models/music_model');

function createMusic(req, res) {
    const body = req.body;
    if (!body.length) {
        return res.status(400).json({ success: false, error: "You must specify an album." });
    }

    const album = new Album(body);
    if (!album) {
        return res.status(400).json({ success: false, error: "Album creation failed." });
    }

    album.save()
        .then(() => {
            return res.status(200).json({ success: true, id: album._id, message: "Album created." });
        })
        .catch(error => {
            return res.status(400).json({ success: false, error: error, message: "Album not created." });
        });
}

async function getAllMusic(req, res) {
    Album.find({}, function (err, albums) {
        if (err) {
            return res.status(400).json({ success: false, error: err });
        }

        if (!albums.length) {
            return res.status(404).json({ success: false, error: "No albums found." });
        }

        return res.status(200).json({ success: true, data: albums });
    });
}

async function getMusicById(req, res) {
    Album.findById(req.params.id, function (err, album) {
        if (err) {
            return res.status(400).json({ success: false, error: err });
        }

        if (!album) {
            return res.status(404).json({ success: false, error: "No albums found." });
        }

        return res.status(200).json({ success: true, data: album });
    });
}

async function updateMusic(req, res) {
    const body = req.body;
    if (!body.length) {
        return res.status(400).json({ success: false, error: "You must provide some data to update." });
    }

    Album.findById(req.params.id, (err, album) => {
        if (err) {
            return res.status(400).json({ success: false, error: err });
        }

        if (!album) {
            return res.status(404).json({ success: false, error: "No albums found." });
        }

        album.album = body.album;
        album.artist = body.artist;
        album.year = body.year;
        album.artwork = body.artwork;

        album.save()
            .then(() => {
                return res.status(200).json({ success: true, id: album._id, message: "Album updated." });
            })
            .catch(error => {
                return res.status(400).json({ success: false, error: error, message: "Album not updated." });
            })
    });
}

async function deleteMusic(req, res) {
    Album.findByIdAndDelete(req.params.id, function (err, album) {
        if (err) {
            return res.status(400).json({ success: false, error: err });
        }

        if (!album) {
            return res.status(404).json({ success: false, error: "No albums found." });
        }

        return res.status(200).json({ success: true, data: album });
    });
}

module.exports = {
    createMusic,
    getAllMusic,
    getMusicById,
    updateMusic,
    deleteMusic
}