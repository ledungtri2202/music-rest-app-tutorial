const express = require('express');
const MusicController = require('../controllers/controller');
const router = express.Router();

router.get('/music', MusicController.getAllMusic);

router.post('/music', MusicController.createMusic);

router.get('/music/:id', MusicController.getMusicById);

router.put('/music/:id', MusicController.updateMusic);

router.delete('/music/:id', MusicController.deleteMusic);

module.exports = router;